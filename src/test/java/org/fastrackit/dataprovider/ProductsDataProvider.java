package org.fastrackit.dataprovider;

import org.fasttrackit.Product;
import org.testng.annotations.DataProvider;

public class ProductsDataProvider {

    @DataProvider(name = "existingItems")
    public Object[][] feedItemsThatExistOnPage() {

        Product gorgeousSoftPizza = new Product("1", "Gorgeous Soft Pizza", "$19.99");
        Product awesomeSoftShirt = new Product("1", "Awesome Soft Shirt", "$29.99");
        Product awesomeMetalChair = new Product("1", "Awesome Metal Chair", "$15.99");

        return new Object[][] {
                {gorgeousSoftPizza},
                {awesomeMetalChair},
                {awesomeSoftShirt}
        };
    }

    @DataProvider(name = "notExistingItems")
    public Object[][] feedItemsThatDoNotExistOnPage() {

        Product pizza = new Product("1", "Underwhelming Hard Pizza", "$19.99");
        Product sweatshirt = new Product("1", "Bad Hard Sweatshirt", "$29.99");
        Product plasticChair = new Product("1", "Bad Plastic Chair", "$15.99");

        return new Object[][] {
                {pizza},
                {sweatshirt},
                {plasticChair}
        };
    }

    @DataProvider(name = "mistypedFirstWordInName")
    public Object[][] feedItemsThatHaveTheFirstWordMistyped() {

        Product gorgeousSoftPizza = new Product("1", "Gorgeouus Soft Pizza", "$19.99");
        Product awesomeSoftShirt = new Product("1", "Awesomee Soft Shirt", "$29.99");
        Product awesomeMetalChair = new Product("1", "Awesssome Metal Chair", "$15.99");

        return new Object[][] {
                {gorgeousSoftPizza},
                {awesomeMetalChair},
                {awesomeSoftShirt}
        };
    }

    @DataProvider(name = "mistypedSecondWordInName")
    public Object[][] feedItemsThatHaveTheSecondWordMistyped() {

        Product gorgeousSoftPizza = new Product("1", "Gorgeous Sodt Pizza", "$19.99");
        Product awesomeSoftShirt = new Product("1", "Awesome Spft Shirt", "$29.99");
        Product awesomeMetalChair = new Product("1", "Awesome Metzl Chair", "$15.99");

        return new Object[][] {
                {gorgeousSoftPizza},
                {awesomeMetalChair},
                {awesomeSoftShirt}
        };
    }

    @DataProvider(name = "mistypedThirdWordInName")
    public Object[][] feedItemsThatHaveTheThirdWordMistyped() {

        Product gorgeousSoftPizza = new Product("1", "Gorgeous Soft Piza", "$19.99");
        Product awesomeSoftShirt = new Product("1", "Awesome Soft Shirrt", "$29.99");
        Product awesomeMetalChair = new Product("1", "Awesome Metal Cair", "$15.99");

        return new Object[][]{
                {gorgeousSoftPizza},
                {awesomeMetalChair},
                {awesomeSoftShirt}
        };
    }

}
