package org.fastrackit.search;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Feature;
import org.fastrackit.config.BaseTestConfig;
import org.fastrackit.dataprovider.ProductsDataProvider;
import org.fastrackit.dataprovider.User;
import org.fastrackit.dataprovider.UserDataProvider;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.LoginModal;
import org.fasttrackit.Product;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

@Feature("Search Field")
public class SearchItemTest extends BaseTestConfig {
    DemoShopPage page;
    @BeforeTest
    public void setup() {
        page = new DemoShopPage();
        page.openDemoShopApp();
    }
    @AfterMethod
    public void reset() {
        Selenide.refresh();
        page.getFooter().resetPage();
    }
    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "existingItems")
    public void searchExistingProductOnPage(Product product) {
        page.getFilterForm().inputSearchFilter(product.getName());
        page.getFilterForm().clickSearch();
        assertTrue(page.areProductsDisplayed());
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "notExistingItems")
    public void searchNotExistingProductOnPage(Product product) {
        page.getFilterForm().inputSearchFilter(product.getName());
        page.getFilterForm().clickSearch();
        assertFalse(page.areProductsDisplayed());
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "mistypedFirstWordInName")
    public void searchProductWithMisstypedFirstWord(Product product) {
        page.getFilterForm().inputSearchFilter(product.getName());
        page.getFilterForm().clickSearch();
        assertFalse(page.areProductsDisplayed());
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "mistypedSecondWordInName")
    public void searchProductWithMisstypedSecondWord(Product product) {
        page.getFilterForm().inputSearchFilter(product.getName());
        page.getFilterForm().clickSearch();
        assertFalse(page.areProductsDisplayed());
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "mistypedThirdWordInName")
    public void searchProductWithMisstypedThirdWord(Product product) {
        page.getFilterForm().inputSearchFilter(product.getName());
        page.getFilterForm().clickSearch();
        assertFalse(page.areProductsDisplayed());
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "existingItems")
    public void searchByFirstWord(Product product) {
        page.getFilterForm().inputSearchFilter(product.getFirstWord());
        page.getFilterForm().clickSearch();
        assertTrue(page.areProductsDisplayed());
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "existingItems")
    public void searchBySecondWordValid(Product product) {
        page.getFilterForm().inputSearchFilter(product.getSecondWord());
        page.getFilterForm().clickSearch();
        assertTrue(page.areProductsDisplayed());
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "existingItems")
    public void searchByThirdWordValid(Product product) {
        page.getFilterForm().inputSearchFilter(product.getThirdWord());
        page.getFilterForm().clickSearch();
        assertTrue(page.areProductsDisplayed());
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "mistypedFirstWordInName")
    public void searchByFirstWordInvalid(Product product) {
        page.getFilterForm().inputSearchFilter(product.getFirstWord());
        page.getFilterForm().clickSearch();
        assertFalse(page.areProductsDisplayed());
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "mistypedSecondWordInName")
    public void searchBySecondWordInvalid(Product product) {
        page.getFilterForm().inputSearchFilter(product.getSecondWord());
        page.getFilterForm().clickSearch();
        assertFalse(page.areProductsDisplayed());
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "mistypedThirdWordInName")
    public void searchByThirdWordInvalid(Product product) {
        page.getFilterForm().inputSearchFilter(product.getThirdWord());
        page.getFilterForm().clickSearch();
        assertFalse(page.areProductsDisplayed());
    }


}