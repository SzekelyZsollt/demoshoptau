package org.fasttrackit;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;

public class FilterForm {
    private final SelenideElement searchButton = $(".btn-sm");
    private SelenideElement searchField = $("#input-search");

    public void clickSearch() {
        this.searchButton.click();
    }
    public void inputSearchFilter(String product){
        searchField.sendKeys(product);
    }
}
