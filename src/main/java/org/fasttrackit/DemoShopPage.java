package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;
import java.util.logging.Filter;

public class DemoShopPage extends Page{
    private Header header;
    private Footer footer;
    private FilterForm filterForm;
    private SelenideElement productsContainer = $(".card");

    public DemoShopPage() {
        this.header = new Header();
        this.footer = new Footer();
        this.filterForm = new FilterForm();
    }

    public Footer getFooter() {
        return footer;
    }

    public Header getHeader() {
        return header;
    }
    public FilterForm getFilterForm() {
        return filterForm;
    }

    public boolean areProductsDisplayed() {
        return productsContainer.isDisplayed();
    }
}
